#include <iostream>
#include <string>
#include <cstdlib>


using namespace std;

class Course
{
	public:
		Course();
		Course(string course_name, int grades[3]);
		string getName();		// the function returns the course name
		int* getGrades();		//the function returns list of grades
		double average();		// the function returns the grades average
		~Course();

	private:
		string _name;
		int _grades[3];
};
