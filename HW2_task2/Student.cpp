#include <iostream>
#include <string>
#include <cstdlib>
#include "Student.h"

using namespace std;

Student::Student()
{

}

Student::Student(string student_name, int number, Course *courses_list)
{
	int i;
	Course *c;
	_name = student_name;
	_NumOfCourses = number;
	_courses_list = new Course[_NumOfCourses];
	for(i = 0; i < _NumOfCourses; i++)
	{
		c = new Course(courses_list[i].getName(), courses_list[i].getGrades());
		_courses_list[i] = *c;
	}
	_randomBonus = rand() % 10 + 1;

}
string Student::getName()
{
	return(_name);
}

double Student::average()
{
	int i;
	double sum = 0;
	int magicBonus = 0;
	for (i = 0; i < _NumOfCourses; i++)
	{
		sum = sum + _courses_list[i].average();
		if (((_courses_list[i]).getName()).back() == 'y')
		{
			magicBonus += 1;
		}
		else if (((_courses_list[i]).getName()).back() == 'e')
		{
			magicBonus += 2;
		}
		else if (((_courses_list[i]).getName()).back() == 'h')
		{
			magicBonus += 5;
		}
	}
	return (((sum + _randomBonus + magicBonus)*1.0)/_NumOfCourses);
}

int Student::getNumOfCourses()
{
	return (_NumOfCourses);
}

string Student::coursesNames()
{
	string courses_names = "";
	int i;
	for (i = 0; i < getNumOfCourses(); i++)
	{
		courses_names = courses_names + " " + _courses_list[i].getName();
	}
	return (courses_names);
}


Student::~Student()
{
	delete[] _courses_list;
}