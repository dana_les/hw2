#include <iostream>
#include <string>
#include "Student.h"

using namespace std;

void sort(Student * arr, int size);
void printData(Student* studentArr, int size);

int main()
{
	cout << "##i have a problem in the 3rd i dont know why it didnt copy all the data of the student in the sort function###" << endl;

	int choice = 0, i;
	Student * studentArr = new Student [0];		// the main array that includes all the data
	Student * newStudentArr;		//in order to add a student to the system
	Student *s;						
	int NumStudents = 0;

	Course *c;
	Course *courses_list;		// in order to create for each student his courses list (names and grades)
	string student_name;
	int student_courses;
	string course_name;
	int grades[3];
	double avg;

	cout << "Welcome to Magshimim School! You have the following options:" << endl << "1 - Add new student to school" << endl << "2 - calculate average grade of all students" << endl << "3 - Show all students descending by their average" << endl << "4 - Exit" << "Enter your choice: " << endl;
	cin >> choice;
	while (choice != 4)
	{
		if (choice == 1)	//new student
		{
			NumStudents++;		//adding a student
			newStudentArr = new Student [NumStudents];		// creating a bigger array for all the students including the new one.
			if (NumStudents >1)
			{
				for (i = 0; i < NumStudents - 1; i++)
				{
					newStudentArr[i] = studentArr[i];		// coping all the previous students' data to the new array
				}
			}
			//the new student data:
			cout << endl << "Enter student's name: ";
			cin >> student_name;									//name
			cout << "Enter number of courses for student: ";
			cin >> student_courses;									//number of courses
			courses_list = new Course[student_courses];
			for (i = 0; i < student_courses; i++)					//all the new student's courses: their names and grades
			{
				cout << endl << "Enter course name: ";
				cin >> course_name;
				cout << "Enter first test grade: ";
				cin >> grades[0];
				cout << "enter secound test grade: ";
				cin >> grades[1];
				cout << "Enter exam grade: ";
				cin >> grades[2];
				c = new Course(course_name, grades);
				courses_list[i] = *c;
			}

			s  =  new Student(student_name, student_courses, courses_list);
			newStudentArr[NumStudents - 1] = *s;

			//copy the new array that include the new one to the origin array
			studentArr = new Student[NumStudents];
			for (i = 0; i < NumStudents; i++)
			{
				studentArr[i] = newStudentArr[i];
			}

			//printing the student name and courses names
			cout << "Student: " << studentArr[NumStudents - 1].getName() << ",  Courses: " << studentArr[NumStudents - 1].coursesNames() << endl << endl;

		}
		else if (choice == 2)	// average of all students
		{
			avg = 0;
			for (i = 0; i < NumStudents; i++)
			{
				avg = avg + studentArr[i].average();
				avg = avg*1.0 / NumStudents;
			}
			cout << "The students average is: " << avg << endl;
		}
		else if (choice == 3)	//printing the array according to average grades
		{
			sort(studentArr, NumStudents);
			printData(studentArr, NumStudents);
		}
		if (choice != 4)	//EXIT
		{
			cout << "Welcome to Magshimim School! You have the following options:" << endl << "1 - Add new student to school" << endl << "2 - calculate average grade of all students" << endl << "3 - Show all students descending by their average" << endl << "4 - Exit" << endl << "Enter your choice: ";
			cin >> choice;
		}
		else
		{
			cout << "Good bye ! :D" << endl;
		}
	}


	system("pause");
	return 0;
}

//the function sort the students array according their average
void sort(Student * arr, int size)
{
	bool not_sorted = true;
	int j , i;
	Student tmp1;
	Student tmp2;
	for (i = 0; i < size; i++)
	{
		for (j = i + 1; j < size; j++)
		{
			if (arr[i].average() < arr[i + 1].average())
			{
				tmp1 = arr[i];
				tmp2 = arr[i + 1];
				arr[i] = tmp2;
				arr[i + 1] = tmp1;
			}
		}
	}
}


// the funtion prints the students array which is sorted - it prints to each student the name and the average
void printData(Student* studentArr, int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		cout << "name: " << studentArr[i].getName() << endl << "average: " << studentArr[i].average() << endl << endl;
	}
}