#include <iostream>
#include <string>
#include <cstdlib>
#include "Course.h"

using namespace std;

class Student
{
	public:
		Student();
		Student(string student_name, int number, Course * courses_list);
		string getName();	//the function returns the student's name
		double average();	//the function returns the grades average of all the courses
		int getNumOfCourses();	// the function return the number of courses the student has
		string coursesNames();	// the function return the courses name
		~Student();

	private:
		string _name;
		int _NumOfCourses;
		Course *_courses_list;
		int _randomBonus;
};
