#include <iostream>
#include <string>
#include <cstdlib>
#include "Course.h"

using namespace std;

Course::Course()
{

}

Course::Course(string course_name, int grades[3])
{
	_name = course_name;
	_grades[0] = grades[0];
	_grades[1] = grades[1];
	_grades[2] = grades[2];
}

string Course::getName()		
{
	return(_name);
}

int* Course::getGrades()
{
	return(_grades);
}

double Course::average()		// the function returns the grades average
{
	return ((_grades[0]*0.25) + (_grades[1]*0.25) +( _grades[2]*0.5) );
}

Course::~Course()
{

}